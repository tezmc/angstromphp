# README

This file last updated 2nd October 2021

## IMPORTANT NOTE

AngstromPHP is currently in the very early days of development, is changing constantly, chaotically, and is not fit to be used for anything. It's just a toy framework at this point.

Take a look at ROADMAP.md, TODO and CHANGELOG.md for what's planned or already done.

If you want something that could be used for something useful, consider waiting for the tagged 1.0 release. At which point the framework will be considered complete.

It's MIT licensed though, so feel free to cut your own foot off or do any other dangerous things you feel like doing, just don't blame me if it goes wrong.

## Getting started

Clone or download and unzip the framwork. Running the serve script (windows .bat or unix .sh) will start the PHP development server from the `public` directory.

Write your application code in the subdirectories of  `/Application`. If you've used MVC frameworks before it should be familiar. Never make a change to anything in `Framework` unless you're explicitly forking it for your application and need to. Consider trying to make a service instead.

It is customary to laugh maniacally when committing code that does something useful.

## About AngstromPHP

AngstromPHP is working towards a fast, minimalist, microframework written in PHP. It is intended solely for creating REST APIs, either for apps with separate frontends (web, mobile etc), or for building microservices.

The focus is on code simplicity, responsiveness and speed of development, minimising the amount of tedious boilerplate busywork needed that keeps a developer away from implementing the actually useful bits of their application.

When complete (version 1.0) it should be possible to get a backend API up and running very quickly, with most of your typing focused on adding application-specific business logic and writing automated tests.

AngstromPHP has no external dependencies, you can either clone the repo or simply unzip a copy of it, init a new repo for your app, and get going without bothering with Composer or Docker or anything like that. You can use it with the PHP internal development testing server to start playing around immediately. A phpunit.xml file is provided for running unit tests of the framework and your applications, you'll need to have phpunit installed.

It will be able to support multiple databases and will intially come with drivers for SQLite and Postgres. If you need others, check out `Framework\Services\Db\Drivers`, you should be able to add one pretty quickly.

To achieve simplicity, it is sometimes a bit opinionated about how things should be named and where they should go. It may not be flexible enough for every possible application you might want to write; but as the framework will be kept small and it's possible to add your own services where you need to it should be good enough for most things.

Some framework services such as logging are performed by the main `Framework\Core\Kernel` class, with a small number of other core classes and utilities where needed. But nearly everything else is a service in `Framework\Services`.

All of your application-specific code goes in the `Application` directory, which has a fixed internal layout.