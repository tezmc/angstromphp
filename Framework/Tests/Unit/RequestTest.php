<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Framework\Core\Request;
use Framework\Services\ValidationUtils;

final class RequestTest extends TestCase
{
    // TODO: Mock instead or don't bother?
    public ValidationUtils $validator;
    
    public function setUp():void
    {
        $this->validator = new ValidationUtils();
    }

    public function getServerDefault(): array
    {
        return [
            'REQUEST_METHOD'    => 'GET',
            'REQUEST_URI'       => '/'
        ];
    }

    public function testParseHome(): void
    {
        $s = $this->getServerDefault();
        $request = new Request($s, $this->validator);
        $this->assertEquals('get', $request->method);
        $this->assertEquals('/', $request->uri);
        $this->assertEquals('v0', $request->apiVersion);
        $this->assertEquals('/', $request->apiRoute);
        $this->assertIsArray($request->apiParams);
        $this->assertEmpty($request->apiParams);
    }

    public function testVersionedApiRequest(): void
    {
        $s = $this->getServerDefault();
        $s['REQUEST_URI'] = '/v1/task';
        $request = new Request($s, $this->validator);
        $this->assertEquals('get', $request->method);
        $this->assertEquals('/v1/task', $request->uri);
        $this->assertEquals('v1', $request->apiVersion);
        $this->assertEquals('/task', $request->apiRoute);
        $this->assertIsArray($request->apiParams);
        $this->assertEmpty($request->apiParams);
    }

    public function testVersionedApiRequestWithParam(): void
    {
        $s = $this->getServerDefault();
        $s['REQUEST_URI'] = '/v1/tasks/3';
        $request = new Request($s, $this->validator);
        $this->assertEquals('get', $request->method);
        $this->assertEquals('/v1/tasks/3', $request->uri);
        $this->assertEquals('v1', $request->apiVersion);
        $this->assertEquals('/tasks/%d', $request->apiRoute);
        $this->assertIsArray($request->apiParams);
        $this->assertEquals(3, $request->apiParams[0]);
    }

    public function testVersionedApiRequestWithMutipleParam(): void
    {
        $s = $this->getServerDefault();
        $s['REQUEST_URI'] = '/v5/user/2/group/3';
        $request = new Request($s, $this->validator);
        $this->assertEquals('get', $request->method);
        $this->assertEquals('/v5/user/2/group/3', $request->uri);
        $this->assertEquals('v5', $request->apiVersion);
        $this->assertEquals('/user/%d/group/%d', $request->apiRoute);
        $this->assertIsArray($request->apiParams);
        $this->assertEquals(2, $request->apiParams[0]);
        $this->assertEquals(3, $request->apiParams[1]);
    }

    public function testPostRequest():void
    {
        // TODO: Add post var checking
        $s = $this->getServerDefault();
        $s['REQUEST_METHOD'] = 'POST';
        $s['REQUEST_URI'] = '/task';
        $request = new Request($s, $this->validator);
        $this->assertEquals('post', $request->method);
        $this->assertEquals('/task', $request->uri);
        $this->assertEquals('v0', $request->apiVersion);
        $this->assertEquals('/task', $request->apiRoute);
        $this->assertIsArray($request->apiParams);
        $this->assertEmpty($request->apiParams);
    }
} 