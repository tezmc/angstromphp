<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Framework\Services\Db\Parser;

final class DbParserTest extends TestCase
{
    public $parser;

    public function setUp():void
    {
        $this->parser = new Parser();
    }

    public function testCreateTableWithNewlines(): void
    {
        $this->markTestSkipped();
        $sql = "CREATE TABLE mytable (\nid INTEGER PRIMARY KEY, \ntitle TEXT\n);\n\n";
        $this->assertTrue($this->parser->parse($sql));
        $schema = $this->parser->schema;
        $this->assertCount(1, $schema->tables);
        $this->assertArrayHasKey("mytable", $schema->table);
        $this->assertIsArray($schema->tables["mytable"]);
    }

    public function testIgnoreComments(): void
    {
        $this->markTestSkipped();
        $sql = "\n\n\n#CREATE TABLE ignoretable (id INTEGER PRIMARY KEY, title TEXT);\n";
        $sql .= "\nCREATE TABLE mytable (\nid INTEGER PRIMARY KEY, \ntitle TEXT\n\t);\n\n";
        $this->assertTrue($this->parser->parse($sql));
        $schema = $this->parser->schema;
        $this->assertCount(1, $schema->tables);
        $this->assertArrayHasKey("mytable", $schema->table);
        $this->assertArrayNotHasKey("ignoretable", $schema->table);
        $this->assertIsArray($schema->tables["mytable"]);
    }
} 