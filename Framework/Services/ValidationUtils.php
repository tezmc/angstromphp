<?php
namespace Framework\Services;

class ValidationUtils
{
	public function isInt($var) {
		if (!is_numeric($var)){ return false;}
		if (!preg_match('/^[0-9]+$/', $var)){return false;}
		return true;
	}
}