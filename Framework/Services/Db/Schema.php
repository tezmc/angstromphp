<?php
namespace Framework\Services\Db;

class Schema
{
    public $tables = [];

    public function getTableNames():array
    {
        return array_keys($this->tables);
    }
}