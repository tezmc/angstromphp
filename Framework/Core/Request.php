<?php
namespace Framework\Core;

use \Framework\Services\ValidationUtils;

class Request
{
    public $get;
    public $post;
    public $server;
    public $cookie;
    public $session;

    public $method; // get/post/put/delete etc.
    public $uri;    // uri requested

    // Version routing
    public $apiVersion = 'v0';
    public $apiRoute = '/';
    public $apiParams = [];

    private ValidationUtils $validationService;

    public function __construct(array $server, ValidationUtils $validationService)
    {
        // $this->get      = $_GET;
        // $this->post     = $_POST;
        $this->server   = $server;
        // $this->cookie   = $_COOKIE;
        // //$this->session = $_SESSION; // re-enable when on server that supports it
        $this->validationService = $validationService;
        $this->method   = strtolower($this->server['REQUEST_METHOD']);
        $this->uri      = strtolower($this->server['REQUEST_URI']);

        $this->parseRoute();
    }

    private function parseRoute()
    {
        $uri = $this->uri;
        $prefix = '/';
        if (substr($uri, 0, strlen($prefix)) == $prefix) {
            $uri = substr($uri, strlen($prefix));
        } 

        $route = explode('/', $uri);
        // if (count($route) == 0){ // '/' index
        //     return;
        // }
        $apiRoute = [];
        foreach($route as $k=>$v) {
            if ($k == 0 && preg_match('/^v[0-9]+$/', $v)) {
                $this->apiVersion = $v;
                continue;
            }
            if ($this->validationService->isInt($v)){
                $this->apiParams[] = (int)$v;
                $apiRoute[] = '%d';
                continue;
            }
            $apiRoute[] = $v;
        }
        $this->apiRoute = '/' . implode('/', $apiRoute);
    }

    public function __toString()
    {
        return "Parsed request = {$this->method}|{$this->apiVersion}|{$this->apiRoute} total_args:". count($this->apiParams);
    }
}