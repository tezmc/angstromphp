<?php
namespace Framework\Core;

use Framework\Core\Request;
use Framework\Core\Response;
use Framework\Services\ValidationUtils;

class Kernel
{

    private array $services = [];
    public array $config;
    public array $routes;

    /**
     * A logging haiku...
     * 
     * three levels only,
     * no notice is noticed and,
     * warnings are errors
     */
    const LOG_DEBUG         = 1;
    const LOG_ERROR         = 2;
    const LOG_FATAL         = 3;
    
    private array $log_names= [
        self::LOG_DEBUG     => 'DEBUG',
        self::LOG_ERROR     => 'ERROR',
        self::LOG_FATAL     => 'FATAL'
    ];

    // Logs are only  written to file at the end of the request if not fatal 
    private array $logs = [];

    public function __construct()
    {
        // TODO: Get rid of config file and use .env instead
        // Load config and routes
        $config_default = require(APP_DIR . "/config-default.php");
        $config_defined = require(APP_DIR . "/config.php");
        $this->config = array_merge($config_default, $config_defined);
        $this->routes = require(APP_DIR . "/routes.php");
        $this->initServices();
    }

    public function __destruct()
    {
        $this->writeLogs(); // Logs only written to file post request
    }

    private function initServices()
    {
        // default services
        $this->services['validationUtils'] = new ValidationUtils();
    }

    public function log(int $level, string $message): void 
    {
        $this->logs[] = date('c') . " {$this->log_names[$level]}: $message";
        return;
    }

    /**
     * Routes the user request to the correct application controller
     */
    public function processRequest() : void
    {
        $request = new Request($_SERVER, $this->service('validationUtils'));
        if ($request->apiVersion == 'v0'){
            $request->apiVersion = $this->routes[$request->apiVersion];
        }

        $this->log(self::LOG_DEBUG, $request);

        if ($routeConfig = $this->getRoute($request)) {
            $this->log(self::LOG_DEBUG, "valid route found for: $request");
            $response = $this->runController($request, $routeConfig);
        } else {
            $this->log(self::LOG_DEBUG, "No route found for: $request");
            $response = new Response();
            $response->setError(404);
        }

        $response->render();        
    }

    private function getRoute(Request $r)
    {
        if (!isset($this->routes[$r->apiVersion][$r->apiRoute][$r->method])) {
            return null;
        }
        return $this->routes[$r->apiVersion][$r->apiRoute][$r->method];
    }

    private function runController(Request $request, array $routeConfig) : Response
    {
        $class = '\\Application\\Controller\\' . $routeConfig['controller'];
        $handler = $routeConfig['handler']; 
        $controller = new $class();
        
        // TODO: Once user auth added, check route requires authentication

        if (method_exists($controller, $handler)){
            $response = $controller->$handler($this, $request);     
        } else {
            $response = new Response();
            $response->setError(
                500, 
                'Request route exists but controller could not be found'
            );
        }

        return $response;
    }

    public function service($key) {
        if (!isset($this->services[$key]))
            throw new \Exception("Service for '$key' not found");
        return $this->service[$key];
    }

    /**
     * Opens the log file and writes the current entries to it
     */
    private function writeLogs(): void
    {
        $filename = date('Ymd') . '-log.txt';
        $file = fopen(DATA_DIR . '/logs/' . $filename, 'a');
        if (!$file) {
            // TODO: What do we do if we can't open a logfile to write to?
            return;
        }
        foreach($this->logs as $m) {
            fwrite($file, $m . PHP_EOL);
        }
        return;
    }
}
