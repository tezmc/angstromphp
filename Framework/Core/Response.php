<?php
namespace Framework\Core;

class Response
{
    public $data    = [];
    public $status  = 200;
    public $isError = false;

    private $response_codes = [
        200 => '200: OK',
        404 => '404: Endpoint not found',
        500 => '500: Internal server error'
    ];

    public function render() : void 
    {
        http_response_code($this->status);
        // TODO: set mimetype
        if ($this->status >= 300) {
            print(json_encode($this->data, JSON_PRETTY_PRINT));
        } else {
            print(json_encode($this->data));
        }
    }

    public function setError(int $code, string $message = '') : void
    {
        $this->code = $code;
        $this->data['error'] = [
            'code'  => $code,
            'msg'   => isset($this->response_codes[$code]) ? $this->response_codes[$code] : 'Unknown error'
        ];
        if (!empty($message)){
            $this->data['error']['info'] = $message;
        }
        $this->isError = true;
    }
}