<?php
namespace Framework\Core;

use Framework\Services\ValidationUtils;

class Router
{
    private $routes;


    public $version;
    public $route;

    public function __construct(array $routes, Request $request, ValidationUtils $validator)
    {
        $this->routes = $routes;
        $this->parseRequest($request);
    }

    private function parseRequest(Request $request):void
    {
        $uri = $request->uri;
        $prefix = '/';
        if (substr($uri, 0, strlen($prefix)) == $prefix) {
            $uri = substr($uri, strlen($prefix));
        } 

        $route = explode('/', $uri);
        // if (count($route) == 0){ // '/' index
        //     return;
        // }
        $apiRoute = [];
        foreach($route as $k=>$v) {
            if ($k == 0 && preg_match('/^v[0-9]+$/', $v)) {
                $this->version = $v;
                continue;
            }
            if ($this->validationService->isInt($v)){
                $request->apiParams[] = (int)$v;
                $apiRoute[] = '%d';
                continue;
            }
            $apiRoute[] = $v;
        }
        $this->route = '/' . implode('/', $apiRoute);
    }
}
