# AngstromPHP developer manual

This manual should contain everything you need to know to start quickly developing applications with AngstromPHP.

As AngstromPHP is currently still under development some of the features discussed here won't exist yet, these will be noted.

## The Framework

### Process flow

After some intial boostrapping - setting some global constants and initialising the autoloader, control passes to the `Framework\Core\Kernel` class. 

The kernel does some initial setup and then calls its `processRequest` method. The request is parsed by the `Framework\Core\Request` and the route parsed by `Framework\Core\Router`. If a valid route is found for the request then control is passed to the relevant controller method in your application for handling. This method must return a `Framework\Core\Response` which is then rendered by the `Kernel`.

## The Application

### Directory structure and required files
