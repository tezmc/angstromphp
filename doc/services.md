# Services

The following services are provided by the kernel. In most instances you need to obtain the service first by calling:

`$kernel->service('serviceName');`

Exceptions to this include Logging, described below.

## Logging

Logs are a service provided by the kernel itself, you do not need to obtain it first by calling `$kernel->service(xxx);`

`$kernel->log($type, $message);`

Logs are stored in an array in the kernel until the request has completed and then written out to file in `data/logs`. The exception to this is when a log message of type `Kernel::LOG_FATAL` is passed, if so, the all log messages received for the request so far are written out to file immediately.

There are three log levels provided, `Kernel::LOG_FATAL`, `Kernel::LOG_ERROR` and `Kernel::LOG_DEBUG`.

## Validation utilities

Various validation tests. An instance of the `Framework\ValidationUtils` class.