# tdm-fram Changelog

## 0.1.0 - unreleased

- APIs are versioned, If no version is given then the mapping to v0 in the routes file is used. EG `'v0' => 'v1'` will use v1 as the default. 
- Routes assume authentication required by default, if a route doesn't require it then `'auth'=>false` should be added to the route definition. However, authentication is not yet implemented.
- Classes autoloaded