# tdm-fram roadmap

## Version 1.0.x

- versioned API routing
- unit testable
- scriptable curl tests with seed data
- $tf CLI tool
- Date helper class (utc<->zone)
- User management and authentication (Token and OAuth)
- User groups and roles
- User<->object Permission system
- Object-based comment system
- Object-based tagging system
- Localisation and locales
- LDAP/Azure user management
- Framework unit test coverage
- Autogenerate repository and model boilerplate code from a db schema object

## Version 2.0.x

- frontend framework for RAD of CRUD sites
- Background task handling
- Event message queue (rabbitMQ or similar?)
- Audit logging (requires events)
- Notifications (requires events)