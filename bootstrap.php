<?php 

// NOTE: no request-specific code here, this needs to work with requests, tests
// and anything else that needs to be able to find classes based on a namespace

// define some directories
define('ROOT_DIR', __DIR__);
define('FRAMEWORK_DIR', ROOT_DIR . '/Framework');
define('APP_DIR', ROOT_DIR . '/Application');
define('DATA_DIR', ROOT_DIR . '/data');
define('PUBLIC_DIR', ROOT_DIR . '/public');

// register autoloader
spl_autoload_register(function($class){
        // TODO: Figure out how this works when your brain is not mush
        $className = ltrim($class, '\\');
        $fileName  = '';
        $namespace = '';
        if ($lastNsPos = strripos($className, '\\')) {
            $namespace = substr($className, 0, $lastNsPos);
            $className = substr($className, $lastNsPos + 1);
            $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
        }
        $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
        if (file_exists($fileName)) {
            require $fileName;
        }
});